## **GET** /profiles

Retrieve the currently available set of profiles. Check the details and use the profile identifiers when creating new encoding jobs.

The internal structure of a profile depends heavily on what the profile produces and this is beyond the scope of this reference. However, the basic (outer) structure of a Profile object is known and looks as follows:

```javascript
{
    /**
     * Unique profile identifier string.
     * Only lowercase letters, digits and hyphens are allowed here.
     */
    "id": "mp4-480p",

    /**
     * Brief description of the profile's purpose.
     */
    "description": "...",

    /**
     * A profile can have one or more task objects.
     */
    "tasks": [
        {
            /**
             * Each task will produce a single file on completion.
             * Currently, task types can be: convert, image or preview.
             */
            "type": "convert",

            // ...
        }

    ]
}
```

<b>Examples</b>

```javascript
/**
 * An video + audio encoding profile using H264+AAC.
 *
 * Outputs:
 *    12318723981723981273_mp4-1080p_01.mp4
 */
{
    "id": "mp4-1080p",

    "tasks": [
        {
            "type": "convert",

            "format": "mp4",

            "video": {
                "size": "x1080",
                "fps": "maximum(30)",
                "codec": "h264",
                "quality": "5",
                "deinterlace": true
            },

            "audio": {
                "codec": "aac+",
                "bitrate": 98304,
                "normalize": true
            }
        }
    ]
}

/**
 * A profile which extracts multiple thumbnails of 150 pixels in height.
 *
 * Outputs:
 *    12318723981723981273_thumbnail-150_01.mp4
 *    12318723981723981273_thumbnail-150_02.mp4
 *    12318723981723981273_thumbnail-150_03.mp4
 */
{
    "id": "thumbnail-150",

    "tasks": [
        {
            "type": "image",
            "label": "01",
            "format": "jpg",
            "position": "25%",
            "size": "x150"
        },
        {
            "type": "image",
            "label": "02",
            "format": "jpg",
            "position": "50%",
            "size": "x150"
        },
        {
            "type": "image",
            "label": "03",
            "format": "jpg",
            "position": "75%",
            "size": "x150"
        }
    ]
}
```

## **GET** /profiles/{id}

> Coming soon.

## **POST** /profiles/{id}

> Coming soon.

## **DELETE** /profiles/{id}

> Coming soon.

---
&copy; Cloud Architectural Inc.
