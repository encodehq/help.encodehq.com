# FAQ

## How are jobs billed?

The price for an encoding job is applied per *minute of output (i.e. "moo")*, rounded-up to the nearest minute.<br />Currently the price is <i>$0.01 / moo</i>.

Jobs which fail during encoding are also billed. However, if a job fails before encoding starts it is not billed. Also:
- Every video preview file is billed as <em>1 minute</em> of output.
- Every 10 thumbnail files are billed as <em>1 minute</em> of output (rounded-up).

<b>Example</b>

A clip of *30 minutes and 14 seconds* encoded in two video versions, with the standard set of thumbnails and a preview will be:
```
( 2 video profiles      * 31 minutes +
  1 thumbnails profile  *  1 minute  +
  1 preview profile     *  1 minute  ) * $0.01/minute
-----------------------------------------------------
  (total)                              = $0.64
```

## How can I test the service?

Any account which does not have valid billing information is automatically is <i>sandbox mode</i>. When in this mode, 
output files are added a `EncodeHQ.com` watermark.

!> No other sandbox-specific limits exist and all other parameters are identical to the paid accounts.

## How fast does job processing start?

Once you submit a new *complete* job (see [Inputs](/jobs?id=inputs)), the job is placed in the processing queue. The amount of time taken for one of our workers to begin processing that job usually varies in the range (0 seconds .. 10 minutes] depending available resources and required scaling.

This strategy is central to how the EncodeHQ engine is able to remain highly-available while also optimizing costs and thus enabling us to keep prices low. If you require SLAs with (sub-)second granularity, please [contact us directly](mailto:we@encodehq.com).

## What is the output FPS rate?

In many cases forcing a specific FPS rate during encoding can produce an unwanted (non-linear) play-back experience therefore we try to leave the input FPS unchanged. However, sometimes an encoding profile may change the original FPS rate, depending on how it's configured.

In such cases, we use a dynamic algorithm to enforce an output FPS rate. Practically, we compute *an exact subdivision (one half, quarter, eighth etc.) of the original FPS, in case the input rate is above 30fps*. The goal is to produce videos with rates of up to 30fps while preserving the fluent motion of the original clip and also improving compression.

---
&copy; Cloud Architectural Inc.
