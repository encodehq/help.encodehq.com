## Lifecycle
It's important to know that during it's lifetime, an encoding job will be in one of these states:

| State      | Description                                                                                                                       | Triggers callbacks |
| ---------- | --------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| `waiting`  | Job was created but it's still waiting for the user to upload the input file (i.e. when the `upload` input type is used).         | No                 |
| `queued`   | Job was placed in the work queue and will be processed by the EncodeHQ engine soon.                                               | Yes                |
| `running`  | The engine has taken the job from the queue and is processing it. A job is not allowed to be in this state for more than 6 hours. | Yes                |
| `complete` | The job was completed successfully and the output files are available for use (according to the `output` type).                   | Yes                |
| `failed`   | The job failed (see `errors` field).                                                                                              | Yes                |

EncodeHQ puts all newly created jobs in the `queued` state straight at creation time, to be immediately eligible for processing.

!> One notable exception to this rule is when the job's [input source](/jobs?id=inputs) is configured as type `upload`. In this case the user is expected to upload the input files *after* the job creation request. See details [here](/jobs?id=upload-url-upload).

## Job Object Structure
The internal representation of a Job is as follows:

```javascript
{
    /**
     * UUID identifier which uniquely identifies this encoding job.
     * Type: String, UUID (v4).
     */
    "id": "c8ced2165f6b40d0b4f4950c0015a168",

    /**
     * Moment in time when the Job object was created.
     * Type: String, UTC ISO time-stamp.
     */
    "created": "2017-02-25T00:34:19.335Z",

    /**
     * Moment of last update of the Job object i.e. last change of state.
     * Type: String, UTC ISO time-stamp.
     */
    "updated": "2017-02-25T00:47:01.982Z",

    /**
     * Current state of the job (see "Lifecycle" for details).
     * Type: String.
     * Possible values: input, queued, running, complete, failed.
     */
    "state": "queued",

    /**
     * The calculated encoding price for this job based on selected profiles
     * and input material duration. Only available in `complete` state!
     */
    "price": 1.28,

    /**
     * List of error strings which try to explain why this job failed.
     * Type: Array[String]. Only available in `failed` state!
     */
    "errors": [],

    /**
     * Encoding profiles to apply on the input file. Value should be `true`  
     * true in order to activate a profile.
     */
    "profiles": [ "mp4-720p", "mp4-1080p", "thumbnails", "preview" ],

    /**
     * Define how will EncodeHQ obtain the input file for processing.
     * See "Inputs" section for available types and options.
     */
    "input": {
        "type": "...",
        // Other fields depend on the type.
    },

    /**
     * Define where will EncodeHQ store the output files after processing.
     * See "Outputs" section for available types and options.
     */
    "output": {
        "type": "...",
        // Other fields depend on the type.
    },

    /**
     * Defines how EncodeHQ will notify you when the job state changes.
     * See "Callbacks" section for available types and options.
     */
    "callback": {
        "type": "...",
        // Other fields depend on the type.
    },

    /**
     * Custom user-data associated with this job (given at creation time).
     */
    "data": {
        // ...
    }

}
```

## Inputs

Currently EncodeHQ supports the following input types (and options):

### Upload URL `upload`

Type `upload` instructs EncodeHQ to add a generated [temporary URL](/jobs?id=temporary-urls) as the `input.url` field in the returned Job object. You must start uploading the input file to this URL using a simple POST request within the first *3 hours* after it's generated, otherwise the URL is lost and you must re-create the job.

When a job is created with this input type, it's set in `waiting` state and will enter `queued` state *immediately after* the file has been successfully uploaded. At that moment EncodeHQ will invoke the `callback` URL to notify that the Job has been completely created and entered `queued` state.

```javascript
// Input field given in the POST body.
"input": {
    "type": "upload"
}

// Temporary URL added by server in the response.
"input": {
    "type": "upload",
    "url": "https://input.encodehq.com/X4LMZys4werKcY5G7..."
}
```

### Amazon S3 Bucket `s3`

Type `s3` instructs EncodeHQ to take the input file from your (custom) AWS S3 bucket. The file is copied from the specified S3 location (see [Job Object Structure](/jobs?id=job-object-structure)) and the job is queued (i.e. is eligible for processing).

```javascript
"input": {
    "type": "s3"
    "bucket": "mySpecialPlace",
    "path": "path/to/some/file.mp4",

    // Access keys...
    "access_key": "...",
    "secret_key": "...",
    // or IAM role...
    "role_arn": "..."
}
```
!> Use either `access_key` and `secret_key` to provide access via AWS keys or the `role_arn` field to provide access via an IAM role; don't mix them! See the [Remote AWS Access](/jobs?id=remote-aws-access) for information on how to define such a role.

## Outputs

Once an encoding job completes *successfully*, the EncodeHQ engine will store the files in the location designated at creation time in the `output` field and will notify the user to retrieve them via the `callback` endpoint. The engine will not store any files in the case of failed jobs.

Currently, EncodeHQ supports the following output types (and options):

### Download URLs `download`

Type `download` ihe Job object's `output` field is populated with temporary URLs for all the generated output files. This updated Job structure is sent to the `callback` endpoint and the user is expected to start downloading the given temporary URLs via simple GET requests within *3 hours* from their creation.

```javascript
// Output field given in the POST body.
"output": {
    "type": "download"
}

// Temporary URLs added by server in the response.
"output": {
    "type": "download",
    "url": "https://output.encodehq.com/X4LMZys4werKcY5G7..."
}
```
### Amazon S3 Bucket `s3`

Type `s3` instructs EncodeHQ to store the output files in your (custom) AWS S3 bucket.

```javascript
"output": {
    "type": "s3"
    "bucket": "mySpecialPlace",
    "path": "prefix/path",

    // Access keys...
    "access_key": "...",
    "secret_key": "...",
    // or IAM role...
    "role_arn": "..."
}
```
!> Use either `access_key` and `secret_key` to provide access via AWS keys or the `role_arn` field to provide access via an IAM role; don't mix them! See the [Remote AWS Access](/jobs?id=remote-aws-access) for information on how to define such a role.

## Callbacks

When [creating a new job](/jobs), you can specify a `callback` endpoint where EncodeHQ should send notifications about changes in the job's state. Whenever the job goes into a new state, EncodeHQ will send the job's updated internal JSON object to the defined callback.

Here are the possible callback types:

### HTTP URL

Type `http` instructs EncodeHQ to make a POST request to the given URL sending the Job object as the body and `Content-Type/Content-Length` as header. You can also define custom headers for this request using the `headers` field (e.g. 'Authorization: <api-key>').

Note that HTTPS URLs are checked for a valid SSL certificate issued by an official authority; you can disable this by setting the `skip_ssl_check` field to `true` (not recommended).

The request should return a *200 or 201* status code in *under 15 seconds*; otherwise, it is retried after *5 minutes, then after 15 minutes*, then dropped.

```javascript
{
    "type": "http",
    "url": "https://my-app.com/callback...",
    "headers": {
        // ...
    },
    "skip_ssl_check": false
}
```

!> Optionally, you could use the custom headers (and data) to implement an extra level of security for every job. E.g. generate a code on your server and pass that as a custom header in the callback definition at job creation. When EncodeHQ calls your URL you can check that header to see if it contains the correct code.

### Amazon SQS

Type `sqs` instructs EncodeHQ to send the Job object as a message payload to your own Amazon SQS Queue.

```javascript
{
    "type": "sqs",
    "url": "https://sqs.us-east-1.amazonaws.com/1234567890/MyQueue",

    // Access keys...
    "access_key": "...",
    "secret_key": "...",
    // or IAM role...
    "role_arn": "..."
}
```
!> Use either `access_key` and `secret_key` to provide access via AWS keys or the `role_arn` field to provide access via an IAM role; don't mix them! See the [Remote AWS Access](/jobs?id=remote-aws-access) for information on how to define such a role.

## Temporary URLs
This is a special kind of URL which is generated by the EncodeHQ API for a *single* and *very specific* upload or download operation. We often use this type of URL to make it easy for you to transfer files to or from the EncodeHQ servers.

A temporary URL expires in *3 hours* from creation and this period can't be extended, so if you don't use it in time you'll have to re-create the entire job. Use the `callback` field to ensure that your application is notified immediately once download URLs are ready.

However, once started, requests to temporary URLs can continue for as long as necessary to complete.

!> A request to a temporary URL does not require authentication or any other headers or body structure, because the API already knows all those details in advance. For this reason *all temporary URLs should be kept secret*!

## Remote AWS Access

In case you want to avoid directly using AWS keys with the EncodeHQ API, you can create a special IAM role with the appropriate permissions (i.e. S3, SQS... depending on the case) and use the role ARN for the API requests (i.e. use the `role_arn` field instead of the `access_key`/`secret_key` fields where appropriate)

!> The EncodeHQ AWS Account ID is `436771119243`, and the provided IAM role must be assumable by this account ID (the principal).

See the [AWS Documentation](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_common-scenarios_third-party.html) for providing third-party AWS accounts with access to your resources.

---
&copy; Cloud Architectural Inc.
