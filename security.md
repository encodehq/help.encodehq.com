# Security

## Use SSL
For security reasons the EncodeHQ API must always be accessed over the `https://` protocol. In fact we don't even guarantee to redirect `http://` to `https://` so just make sure you always use the SSL secured protocol.

Moreover, all our endpoints will always be fronted by officially recognized SSL certificates, so please do not be tempted to skip the SSL certificate checks when contacting the EncodeHQ API from code/scripts.  

## Authentication

With the exception of the `/help` and `/status` endpoints, all requests to the EncodeHQ API must be authenticated, otherwise they are rejected. A request is authenticated using the `Authorization: <api-key>` header with a valid API key as the value.

API keys can be created/managed on your [account page](https://encodehq.com/account). You should create a separate API Key for each application which will use the EncodeHQ API (you can define up to **100 API Keys**).

```bash
# Example: get available encoding profiles.
curl --header "Authorization: avbKF7O5BVjzJ1t2_tcyMTqZIbRY9Ti91IGIO9gW20Hj8dc6y" \
    "https://api.encodehq.com/profiles"
```

## Throttled access
By default all clients are limited to a maximum of **10 jobs running simultaneously**. Exceeding jobs are refused with an `HTTP 503` error.

For paid accounts the limit is of **100 jobs running simultaneously**. To change this limit please write us at [we@encodehq.com](mailto:we@encodehq.com) describing your request.

---
&copy; Cloud Architectural Inc.
