# Profiles

An encoding Job consists of performing one or more transcoding operations on the input file according to a configuration; we call this operation *applying a profile* on the input file.

!> A profile represents the set of preferences regarding a specific encoding operation such as *a/v codecs, container format, resolution, quality, bitrate* and others, or (in the case of thumbnail profiles) things like *number of thumbnails to extract, thumbnail sizes* etc.

EncodeHQ offers a predefined set of profiles which are instantly available to all users. However you will also be able to create your own (private) profiles. Remember that unlike globally defined Profiles, all private Profiles must have their IDs prepended by a tilde ('~') character.

A Job may contain from 1 to 10 profiles at once. When successfully applied, each profile will generate a separate set of (one or more) files and all generated filenames will carry the following generic prefix:

```bash
# Prefix:
<jobId> _ <profileId> [ _ <label> ] . <extension>

# Examples:
c8ced2165f6b40d0b4f4950c0015a168_mp4-1080p.mp4
c8ced2165f6b40d0b4f4950c0015a168_thumbnails_x125.jpg
c8ced2165f6b40d0b4f4950c0015a168_thumbnails_x200.jpg
```

## Profile Object Structure

> Coming soon.

## Tasks

> Coming soon.

## Confiuration Options

> Coming soon.

---
&copy; Cloud Architectural Inc.
