## **GET** /jobs

Returns a listing with encoding jobs issued by the authenticated account in *reverse chronological order* of their creation date.

By default only 100 jobs are returned. Use the `limit` and `offset` query parameters to paginate the results; note that the `limit` is always truncated at `1000`.

Also, you can use the `state` query parameter to only return the jobs which are in certain states at the moment of the call (e.g. `/jobs?states=queued,runnning`).

**Request**

- Query parameters:
    - `limit` *(optional)* - Size of returned job listing *(default=100, range=[1..1000])*.
    - `offset` *(optional)* - Start listing from this position *(default=0)*.
    - `states` *(optional)* - Comma-separated list of states to filter results by.

- Headers:
    ```http
    Authorization: <api-key>
    ```

**Response**
- `200 OK` (expected)
    - Return a listing of Job objects issued by your account.
    - *Headers*: `Content-Type: application/json`
    - *Body*: an array of [Job objects]/jobs?id=job-object-structure) (can be empty).
- `400 BAD REQUEST`
    - Parameters are invalid.

### Example

Get last 20 jobs in either queued or running states.

**Sample request**

```bash
curl --header "Authorization: avbKF7O5BVjzJ1t2_tcyMTqZIbRY9Ti91IGIO9gW20Hj8dc6y" \
    "https://api.encodehq.com/jobs?limit=20&states=queued,running"
```

**Sample response**

```http
200 OK
Content-Type: application/json
```
```javascript
[
    {
        "id": "c8ced2165f6b40d0b4f4950c0015a168",
        "created": "2017-02-25T00:34:19.335Z",
        "updated": "2017-02-25T00:34:19.335Z",
        "state": "queued",
        // ...
    },
    // ...
]
```

## **GET** /jobs/{id}

Returns the Job object corresponding to the given `id`. See [the Job structure](/jobs?id=job-object-structure) for details on the contained fields.

**Request**

- URL parameter:
    - `{id}` *(required)* - Identifier string for the job.

- Headers:
    ```http
    Authorization: <api-key>
    ```

**Responses**
- `200 OK` (expected)
    - Returns the Job object.
    - *Headers*: `Content-Type: application/json`
    - *Body*: [JSON representation of the Job](/jobs?id=job-object-structure).
- `404 NOT FOUND`
    - Job ID was not found.

### Example

**Sample request**

```bash
curl --header "Authorization: avbKF7O5BVjzJ1t2_tcyMTqZIbRY9Ti91IGIO9gW20Hj8dc6y" \
    "https://api.encodehq.com/jobs/c8ced2165f6b40d0b4f4950c0015a168"
```

**Sample response**

```http
200 OK
Content-Type: application/json
```
```javascript
{
    "id": "c8ced2165f6b40d0b4f4950c0015a168",
    "state": "running",
    "errors": [],
    // ...
}
```

## **POST** /jobs

Create a new encoding job.

This endpoint expects a `JobDefinition` JSON structure describing the new job's characteristics, and (if successful) returns the created `Job` structure.

You can also associate custom data to a job by using the `data` field at creation time. This field is carried as-is within the JSON representation of the job throughout its lifetime, so this custom data will be available to all callbacks and queries alike.

**Request**

- Query parameters: none.

- Headers:
    ```http
    Authorization: <api-key>
    Content-Type: application/json
    ```

- Body:
    ```javascript
    /**
     * JobDefinition object.
     */
    {
        /**
         * Selection of encoding profiles IDs to apply on the input file.  
         * Remember that private profile IDs are prepended by a .
         * Required: YES.
         */
        "profiles": [ "mp4-720p", "mp4-1080p", "#custom-thumbs" ],

        /**
         * Define how EncodeHQ should acquire the input file for processing.
         * Each type has a specific structure; see "Inputs" for options.
         * Required: YES.
         */
        "input": {
            "type": "...",
            // Other fields depend on the type.
        }

        /**
         * Define where will EncodeHQ store the output files after processing.
         * Each type has a specific structure; see "Outputs" for options.
         * Required: YES.
         */
        "output": {
            "type": "...",
            // Other fields depend on the type.
        },

        /**
         * Define how EncodeHQ will notify you when the job's state changes.
         * Each type has a specific structure; see "Callbacks" for options.
         * Required: NO (but highly recommended).
         */
        "callback": {
            "type": "...",
            // Other fields depend on the type.
        },

        /**
         * Custom user data for this job (always present in the Job object).
         * Required: NO.
         */
        "data": {
            // ...
        }

    }
    ```

**Response**
- `201 Created` (expected)
    - This means the `Job` was successfully created in either `queued` or `waiting` state (see [Lifecycle](/jobs?id=lifecycle) for an explanation).
    - *Response headers*: `Content-Type: application/json`
    - *Response body*: [internal object](jobs?id=job-object-structure) created for the new Job.
- `400 BAD REQUEST`
    - Job definition is invalid.

### Example

Create new job by uploading the input file and instructing EncodeHQ to store outputs in a custom S3 location.

**Sample request**

```http
Authorization: avbKF7O5BVjzJ1t2_tcyMTqZIbRY9Ti91IGIO9gW20Hj8dc6y
Content-Type: application/json
```

```javascript
{
    "input": {
        "type": "upload"                    // I want to upload the input file.
    },
    "output": {
        "type": "s3",                       // Store outputs in this S3 bucket.
        "bucket": "my-own-bucket",
        "path": "prefix/path/",
        "access_key": "...",
        "secret_key": "..."
    }
    "profiles": [                           // Apply two profiles.
        "mp4-1080p",
        "thumbnail-set"
    ],
    "callback": {
        "type": "http"                      // Notify me at this URL.
        "url": "https://myapp.com/..."
    },
    "data": {
        "foo": "buzz"
    }
}
```

**Sample response**

```http
201 Created
Content-Type: application/json
```
```javascript
{

    "id": "abc...",                         // Job ID string.
    "state": "waiting",                     // Job is waiting for upload.
    "errors": [],                           // No errors for now.

    "created": "2017-02-25T00:34:19.335Z",  // UTC ISO creation time.
    "updated": "2017-02-25T00:47:01.982Z",  // UTC ISO last update time.

    "input": {
        "type": "upload",        

        // Temporary upload URL added by the server;
        // this is where you must upload the input file.
        "url": "https://input.encodehq.com/X4LMZys4werKcY5G7..."
    },

    // Everything else is kept (intact) from the request body.
    "output": {
        "bucket": "my-own-bucket",
        "path": "prefix/path/",
        "access_key": "...",
        "secret_key": "..."
    },

    "profiles": [
        "mp4-1080p",
        "thumbnail-set"
    ],
    "callback": {
        "type": "http",
        "url": "https://myapp.com/..."
    },
    "data": {
        "foo": "buzz"
    }
}
```

## **DELETE** /jobs

!> Not supported!

---
&copy; Cloud Architectural Inc.
